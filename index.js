db.users.insertMany([
	{
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "666999",
		email: "jing@goy.com"
	},
	courses: ["PHP", "React", "Python"],
	department: "none"
	},

	{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 70,
	contact: {
		phone: "5347786",
		email: "francism@yahoo.com"
	},
	courses: ["React", "Laravel", "SASS"],
	department: "HR department"
	}

])


//Find users with letter s in their first name or d in their last name


db.users.find({
	$or: [
		{
			firstName:{$regex: 's', $options:'$i'}
		},
		{
			firstName:{$regex: 'd', $options:'$i'}
		}
]
})

//Find users who are from the HR department and their age is greater than or equal to 70

db.users.find({
	$and:[
		{
			department:"HR department"
		},
		{
			age:{lte:70}
		}
	]
})


//find users with the letter e in their first name and has ana gae of less than or equal to 30

db.users.find({
	$and:[
		{
			firstName:{$regex: 'e'}
		},

		{
			age:{$lte:30}
		}
	]
})